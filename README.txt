
user_autorole: Assign roles to users upon registration.

Features:
* Administrator can define a set of inital roles for new users
* Simple to use
* No new tables needed (Safe to delete at any time)

Advanced features:
* Allow users to choose a "role set" upon account creation (obviously, only roles the Administrator has decreed to be acceptable for new users)
* Quick registration as a role set with the "Enable register links" option.

How to use:

1. Install and enable the module.
2. Go to admin/settings/user_autorole.
3. Set the Automatic Roles. These will be applied to all new users upon registration.

Advanced use:
- Check the "Enable register links" option if you will be using rolesets and wish to be able to use user/register/(roleset) to bypass the normal autorole form.
- Use the "add a roleset" button to create a new roleset.
- The 'Short name' is used as the last part of the register link if you decided to enable it. It is also the identifier that the module uses to tell rolesets apart. The Full name is the name that is displayed on the registration form dropdown. I suggest using one word for the 'Short name.'

TODO:
- Decide what terminology to use, and be consistent. i.e. roleset vs role set.
- Add confirmation for roleset deletion.
- Drupal 5 support.

This module was inspired by the work done by Project Opus described at http://docs.projectopus.com/node/21

