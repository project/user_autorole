<?php

/**
 * @file
 * User Autorole.
 * Allows the administrator to add additional roles to users upon registration.
 */

/**
 * Implementation of hook_help().
 */
function user_autorole_help($section) {
  switch ($section) {
    case 'admin/modules#description':
      return t('Allows additional roles to be added to users upon registration.');
      break;
  }
}

/**
 * Implementation of hook_perm().
 */
function user_autorole_perm() {
  return array('Change autorole settings');
}

/**
 * Implementation of hook_menu().
 */
function user_autorole_menu($may_cache) {
  if ($may_cache && user_access('Change autorole settings')) {
    $menu = array();

    $menu[] = array(
      'path' => 'admin/settings/user_autorole',
      'title' => t('user_autorole'),
      'callback' => 'user_autorole_settings_form',
      'access' => user_access('Change autorole settings'),
      'type' => MENU_NORMAL_ITEM,
    );

    $menu[] = array(
      'path' => 'admin/settings/user_autorole/settings',
      'title' => t('settings'),
      'weight' => -1,
      'type' => MENU_DEFAULT_LOCAL_TASK,
    );

    $menu[] = array(
      'path' => 'admin/settings/user_autorole/add',
      'title' => t('add a roleset'),
      'callback' => 'user_autorole_add_roleset',
      'type' => MENU_LOCAL_TASK,
    );

    $menu[] = array(
      'path' => 'admin/settings/user_autorole/delete',
      'title' => t('delete a roleset'),
      'callback' => 'user_autorole_delete_roleset',
      'type' => MENU_LOCAL_TASK,
    );

    return $menu;
  }
}

/**
 * Implementation of hook_user().
 */
function user_autorole_user($op, &$edit, &$account, $category = NULL) {
  switch ($op) {

    case 'insert':
      // Make a list of roles to apply, starting with the default roles.
      $roles_to_add = variable_get('user_autorole_roles',array());

      // Add roles from the selected roleset.
      $rolesets = variable_get('user_autorole_rolesets',array());
      if (isset($rolesets[$edit['autorole']])) {
        $roles_to_add = array_merge($rolesets[$edit['autorole']]['roles'],$roles_to_add);
        watchdog('user_autorole',t('New user %name registered into role set %roleset', array(
          '%name' => theme('placeholder', $edit['name']),
          '%roleset' => theme('placeholder', $rolesets[$edit['autorole']]['name']))), WATCHDOG_NOTICE);
      }
 
      // Remove duplicates.
      $roles_to_add = array_unique($roles_to_add);

      $roles = _user_autorole_get_roles();
      foreach ($roles_to_add as $role) {
        watchdog('user_autorole',t('Adding role %role to new user %name.', array('%name' => theme('placeholder', $edit['name']), '%role' => theme('placeholder', $roles[$role]))),WATCHDOG_NOTICE);
        db_query("INSERT INTO {users_roles} (uid,rid) VALUES (%d,%d)",$edit['uid'],$role);
        //array_push($account['roles'],$role);
      }
      break;

    case 'register':
      $rolesets = _user_autorole_get_roleset_names();

      if (!empty($rolesets)) {
        $form = array();

        $showform = TRUE;

        if (variable_get('user_autorole_shortcut',FALSE)) {
          if ( (arg(0)=='user') && (arg(1)=='register') && arg(2) ) {
            $poss = strtolower(arg(2));
            if (array_key_exists($poss,$rolesets)) {
              // Hardcode the value if it was given in the URL.
              $form['autorole'] = array(
                '#type' => 'value',
                '#value' => $poss,
              );
              $showform = FALSE;
            }
          }
        }
        if ($showform) {

          $form['user_autorole'] = array(
            '#type' => 'fieldset',
            '#title' => t('Account purpose'),
          );
          $form['user_autorole']['autorole'] = array(
            '#type' => 'select',
            '#title' => t('Account purpose'),
            '#options' => $rolesets,
            '#default_value' => $edit['autorole'],
            '#description' => t('Choose a purpose for this account.'),
          );
        }
        return $form;
      }
      break;
  }
}

/**
 * Implementation of hook_settings().
 */
function user_autorole_settings_form() {

    $roles = _user_autorole_get_roles();

    $form['user_autorole'] = array(
      '#type' => 'fieldset',
      '#title' => t('User_autorole settings'),
    );

    $form['user_autorole']['general'] = array(
      '#type' => 'fieldset',
      '#title' => t('Behavior'),
    );

    $form['user_autorole']['general']['enable_shortcut'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable register links'),
      '#default_value' => variable_get('user_autorole_shortcut',FALSE),
      '#description' => t('This will enable registering into a roleset by using a special link, in the form user/register/(shortname)'),
    );

    $form['user_autorole']['user_autorole_roles'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Automatic Roles'),
      '#default_value' => variable_get('user_autorole_roles',array()),
      '#multiple' => TRUE,
      '#options' => $roles,
      '#tree' => FALSE,
      '#description' => t('Select the roles you wish to assign to new users.'),
    );

    $rolesets = variable_get('user_autorole_rolesets',array());

    if(!empty($rolesets)) {

      $form['user_autorole']['rolesets'] = array(
        '#type' => 'fieldset',
        '#title' => t('Role sets'),
      );

      foreach ($rolesets as $name=>$roleset) {
        $form['user_autorole']['rolesets']['autorole_form_'.$name] = array(
          '#type' => 'fieldset',
          '#title' => t('%name (%shortname)',array('%shortname'=>theme('placeholder',t($name)), '%name' => theme('placeholder',t($roleset['name'])))),
        );

        $form['user_autorole']['rolesets']['autorole_form_'.$name]['autorole_'.$name] = array(
          '#type' => 'checkboxes',
          '#default_value' => $roleset['roles'],
          '#multiple' => TRUE,
          '#options' => $roles,
          '#tree' => FALSE,
          '#description' => t('Select the roles this roleset will assign when selected by a new user.'),
        );
      }
    }

    $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration') );
    $form['buttons']['reset'] = array('#type' => 'submit', '#value' => t('Reset to defaults') );

    return drupal_get_form('user_autorole_settings_form',$form);
}

function user_autorole_settings_form_submit($form_id, $form_values) {
  // Get the op so we can tell if we're resetting or not.
  $op = isset($_POST['op']) ? $_POST['op'] : '';

  if ($op==t('Reset to defaults')) {
    variable_del('user_autorole_shortcut');
    variable_del('user_autorole_roles');
    variable_del('user_autorole_rolesets');
  }
  else {

    variable_set('user_autorole_shortcut',$form_values['enable_shortcut']);

    // Set the automatic roles
    variable_set('user_autorole_roles',array_filter($form_values['user_autorole_roles']));

    $rolesets = variable_get('user_autorole_rolesets',array());
    $newrolesets = $rolesets;

    foreach ($rolesets as $name=>$roleset) {
      if (isset($form_values['autorole_'.$name])) {
        $newrolesets[$name]['name'] = $roleset['name'];
        $newrolesets[$name]['roles'] = array_filter($form_values['autorole_'.$name]);
      }
    }
    variable_set('user_autorole_rolesets',$newrolesets);

  }
} 

/**
 * Private function to get a list of the roleset names.
 */
function _user_autorole_get_roleset_names($none = TRUE) {
  $rolesets = array();

  if ($none) {
   $rolesets[] = t('<none>');
  }

  $roleset_names = variable_get('user_autorole_rolesets',array());

  foreach ($roleset_names as $name => $roleset) {
    $rolesets[$name] = t($roleset['name']);
  }

  return $rolesets;
}

function _user_autorole_get_roles() {
  $roles = user_roles(TRUE);
  // Remove 'authenticated users' from the list.
  unset($roles[DRUPAL_AUTHENTICATED_RID]);
  return $roles;
}


/**
 * Callback for add roleset form.
 */
function user_autorole_add_roleset() {
  $form['shortname'] = array(
    '#type' => 'textfield',
    '#title' => t('Short name'),
    '#required' => TRUE,
    '#description' => t('This name will be used to form the "sign up as" link and to identify the roleset internally. Any uppercase characters will be converted to lower case. For best results, use a single word.'),
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Full name'),
    '#required' => TRUE,
    '#description' => t('The name entered will be used on the user registration form when using the regular registration form.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('add'),
  );

  return drupal_get_form('user_autorole_add_roleset',$form);
}

/**
 * Submit hook for add roleset form.
 */
function user_autorole_add_roleset_submit($form_id, $form_values) {
  $rolesets = variable_get('user_autorole_rolesets',array());
  $rolesets[strtolower($form_values['shortname'])] = array('name'=>$form_values['name']);
  variable_set('user_autorole_rolesets',$rolesets);
}

/**
 * Callback for delete roleset form.
 */
function user_autorole_delete_roleset() {

  $rolesets = _user_autorole_get_roleset_names(TRUE);

  $form['role'] = array(
    '#type' => 'select',
    '#title' => t('Roleset to delete'),
    '#options' => $rolesets,
    '#description' => t('Choose the roleset to delete.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );

  return drupal_get_form('user_autorole_delete_roleset',$form);
}

/**
 * Submit hook for delete roleset form.
 */
function user_autorole_delete_roleset_submit($form_id, $form_values) {
  if (!empty($form_values['role'])) {
    $rolesets = variable_get('user_autorole_rolesets',array());
    unset($rolesets[$form_values['role']]);
    variable_set('user_autorole_rolesets',$rolesets);
  }
}
